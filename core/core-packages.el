;;; core-pacakges.el --- Packages needed by everything -*- lexical-binding: t; -*-

(defun bebop--init-core-packages ()
  "Initialize every packages needed by all packages:
   1. `use-package'
   2. `key-chord'
   3. `general'"

  (bebop--init-use-package)

  (use-package key-chord
    :commands key-chord-mode)
  (use-package general
    :init
    (setq general-override-states '(insert
				    emacs
				    hybrid
				    normal
				    visual
				    motion
				    operator
				    replace))
    :config
    ;; Define leader keys for general
    (defcustom bebop-leader "SPC"
      "Keybinding for commands in any mode"
      :type 'key-sequence
      :group 'bebop-variables)
    (defcustom bebop-local-leader ","
      "Keybinding for major mode commands"
      :type 'key-sequence
      :group 'bebop-variables)

    ;; These "definer"s prevent us from repeating
    (general-create-definer bebop-leader-def
      :prefix bebop-leader
      :keymaps 'override
      :states '(normal visual motion))

    (general-create-definer bebop-local-leader-def
      :prefix bebop-local-leader
      :major-modes t
      :states '(normal visual motion))

    ;; Unbind SPC and give it a title for which-key (see echo area)
    (bebop-leader-def
      "" '(nil :which-key "Bebop Commands"))

    (bebop-local-leader-def
      :keymaps 'override
      "" '(nil :which-key "Local Leader"))))

(defun bebop--init-use-package ()
  "Initializes `use-package'"
  (eval-when-compile
    (require 'use-package)))

(provide 'core-packages)
;;; core-packages.el ends here
