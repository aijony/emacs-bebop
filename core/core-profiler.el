;;; core-profiler.el --- Simple timing -*- lexical-binding: t; -*-

;; Logger

(defun bebop--time-init ()
  (defvar init-gcs gcs-done)
  (defvar init-time (float-time
                     (time-subtract after-init-time before-init-time)))
  (message "Emacs ready in %s with %d garbage collections."
           (format "%.3f seconds" init-time) init-gcs))

(defun bebop--time-load ()
  (defvar load-gcs gcs-done)
  (defvar load-time (float-time
                     (time-subtract (current-time) before-init-time)))
  (message "Emacs loaded in %s with %d garbage collections."
           (format "%.3f seconds" load-time) load-gcs))

(defun bebop-retrieve-startup-times ()
  (interactive)
  (print (concat (shell-command-to-string
                  "cd ~/.emacs.d && git log --pretty=format:'%h' -n 1") ", "
                  (format "%.3f" init-time) ", "
                  (format "%d" init-gcs) ", "
                  (format "%.3f" load-time) ", "
                  (format "%d" load-gcs))))

(provide 'core-profiler)
;;; core-profiler.el ends here
