;;; core-ui.el --- magnifique -*- lexical-binding: t; -*-

(defcustom bebop-center-screen nil
  "Whether or not to center Emacs frames"
  :type 'boolean
  :group 'bebop-variables)

(defcustom bebop-start-theme 'tango-dark
  "Theme to be used for Emacs Bebop, one may also cons the package to
find the theme as such: '(cool-theme . cool-package)."
  :type '(choice symbol cons)
  :group 'bebop-variables)

(defcustom bebop-font nil
  "Font to be used for Emacs Bebop.
  Examples:
  - \"Terminus\"
  - \"Fira Mono-14\""
  :type '(choice boolean string)
  :group 'bebop-variables)

(defcustom bebop-gui-extra -1
  "Calls `menu-bar-mode', `toggle-scroll-bar', and `tool-bar-mode'
  with `bebop-gui-extra' as the parameter."
  :type 'integer
  :group 'bebop-variables)

(defun bebop--load-gui ()
  ;; Enable GUI junk
  (if (eq bebop-gui-extra 1)
      (progn
        (menu-bar-mode bebop-gui-extra)
        (toggle-scroll-bar bebop-gui-extra)
        (tool-bar-mode bebop-gui-extra)))

  (when bebop-start-theme
    (if (listp bebop-start-theme)
        (progn
          (require (car bebop-start-theme))
          (load-theme (cdr bebop-start-theme)))))

  (set-frame-font bebop-font nil t)

  (add-hook 'command-line-functions #'bebop--inhibit-startup-screen)

  (if bebop-center-screen
      (bebop/set-initial-frame)))

(provide 'core-ui)
;;; core-ui.el ends here
