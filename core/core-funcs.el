;;; core-funcs.el --- λx.x -*- lexical-binding: t; -*-
(require 'cl-lib)

;; Macros are basically functions :/
(defmacro bebop//ghost (&rest body)
  "Evaluate BODY, with `message' doing anything"
  `(cl-letf (((symbol-function 'message)
              (lambda (&rest args)
                nil)))
     (let ()
       (progn ,@body))))

(defun bebop--inhibit-startup-screen ()
  "Inhibit startup screen when called before Emacs fully loads"
  (ignore (setq inhibit-startup-screen t)))

;; Set initial frame size and position
(defun bebop/set-initial-frame ()
  (setq frame-resize-pixelwise t)
  (let* ((base-factor 0.70)
         (a-width (* (display-pixel-width) base-factor))
         (a-height (* (display-pixel-height) base-factor))
         (a-left (truncate (/ (- (display-pixel-width) a-width) 2)))
         (a-top (truncate (/ (- (display-pixel-height) a-height) 2))))
    (set-frame-position (selected-frame) a-left a-top)
    (set-frame-size (selected-frame) (truncate a-width)  (truncate a-height) t)))

(provide 'core-funcs)
;;; core-funcs.el ends here
