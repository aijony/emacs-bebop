;;; core-optimize.el --- Speed after init -*- lexical-binding: t; -*-

(defun bebop-defer-garbage-collection-h ()
  "Defer it so that commands launched immediately after will enjoy the
 benefits."
  (setq gc-cons-threshold bebop-gc-cons-threshold))

(defun bebop-restore-garbage-collection-h ()
  "Restore gc back to normal"
  (run-at-time
   1 nil (lambda () (setq gc-cons-threshold bebop-gc-cons-threshold))))

(defun bebop--optimize ()
  ;; Minibuffer can be very garbage intensive
  (add-hook 'minibuffer-setup-hook #'bebop-defer-garbage-collection-h)
  (add-hook 'minibuffer-exit-hook #'bebop-restore-garbage-collection-h)
  ;; in ~/.emacs.d/init.el (or ~/.emacs.d/early-init.el in Emacs 27)
  (setq package-enable-at-startup nil)) ; don't auto-initialize!

(provide 'core-optimize)
;;; core-optimize.el ends here
