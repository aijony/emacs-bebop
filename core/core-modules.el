;;; core-modules.el --- Meta-packages -*- lexical-binding: t; -*-

(defconst bebop--module-files
  '(funcs config keybindings packages)
  "The files (in order) to load a module.
  `funcs': Pre-load functions needed for setup
  `config' Pre-load configurations
  `keybindings' Pre-load configuration and new keybindings
  `packages': Load configuration for a file and the packages loaded")

(defvar bebop--unloaded-modules '()
  "Modules that weren't been loaded")

(defvar bebop--loaded-modules '()
  "Modules that have been loaded")

(defvar bebop-modules-async nil
  "If t, spawn a thread for every module loaded.
This is good for testing purposes (i.e. scrambling the load order),
and maybe someday useful if Emacs becomes multi-threaded. However, simply
loading all modules linearly in a thread is probably the
better choice.")

(defvar bebop--modules-loading 0
  "Number of modules currently loading")

(defvar bebop-modules-loaded-hook nil
  "Hook for after modules have been loaded")

(defvar bebop--module-mutexes
  "A hashtable of all the module mutexes"
  nil)

(defvar bebop--modules-start (make-mutex "modules-start"))

(defun bebop--assign-mutexes (module)
  (let ((mutex (make-mutex module)))
    (puthash module mutex bebop--module-mutexes)))

(defun bebop--load-modules (modules)
  "Helper functions to load modules specified by `modules-list' and
 in `bebop--modules-directories'. This may be run as a thread
(note that it will create multiple sub-threads)."
  (add-hook 'bebop-modules-loaded-hook #'bebop--notify-if-unloaded)
  ;; Only need to reload if modules were asynchronously loaded
  (when bebop-modules-async
    (setq bebop--module-mutexes
          (make-hash-table
           :test 'equal
           ;; The table will be removed later
           :weakness 'nil
           :size bebop--modules-total))
    (mapc #'bebop--assign-mutexes modules)
    (add-hook 'bebop-modules-loaded-hook
              (lambda () (setq bebop--module-mutexes nil)))
    (add-hook 'bebop-modules-loaded-hook #'bebop--reload-buffers-t))
  (mutex-lock bebop--modules-start)
  (mapc #'bebop--load-module-t modules)
  (mutex-unlock bebop--modules-start)
  t)

(defun bebop--modules-loaded ()
  "Checks if `bebop--modules-loading' to be zero, then runs
 `bebop--modules-loaded-hook'."
  (when (eq 0 bebop--modules-loading)
    (setq bebop--modules-loading -1)
    (run-hooks 'bebop-modules-loaded-hook)))

(defun bebop--notify-if-unloaded ()
  "Notifies if bebop--unloaded-modules is non-nil"
  (if bebop--unloaded-modules
      (message "The following modules remain unloaded: %s"
               bebop--unloaded-modules)
    (when bebop-developer
      (message "All modules loaded"))))

(defun bebop--reload-buffers-t ()
  "Helper function for `bebop--reload-buffer' to run each buffer
                                reload as a thread"
  (dolist (buffer (buffer-list))
    (make-thread (apply-partially #'bebop--reload-buffer buffer))))

(defun bebop--reload-buffer (&optional buffer)
  "Reload config for open files"
  (if (not buffer)
      (setq buffer (current-buffer)))
  (let ((file (buffer-file-name buffer)))
    (when (or (and file (file-readable-p file))
              (eq buffer (get-buffer "*scratch*")))
      (with-current-buffer buffer
        (with-demoted-errors "Error: Cannot apply config to %S"
          (normal-mode))))))

(defun bebop--load-module-t (module)
  "Helper function for `bebop-load-module' that makes a thread if
                                `bebop-modules-async'"
  ;; This is decremented in `bebop--load-module'
  (bebop--modules-loading-adjust 1)
  (if bebop-modules-async
      (message (thread--blocker
                (make-thread (apply-partially #'bebop--load-module module))))
    (bebop--load-module module)))

(defun bebop--module-loadedp (module)
  "Returns whether a module has been loaded"
  (member module bebop--loaded-modules))

(defun bebop--load-module (module)
  "Loads a module's files defined by `bebop--modules-files' If it
      finds a loads a file in the  folder it removes it from the
      `bebop--unloaded-modules' list. This may be run as a thread
      (see `bebop--load-module-t')."
  (when bebop-modules-async
    (mutex-lock bebop--modules-start)
    (mutex-lock (gethash module bebop--module-mutexes))
    (mutex-unlock bebop--modules-start))
  (when (or bebop-modules-async (not (bebop--module-loadedp module)))
    (let ((loaded-mod-b t))
      ;; Load all possible files
      (dolist (file bebop--module-files)
        (setq loaded-mod-b
              ;; Don't let a nil overwrite a t
              (or
               (bebop--load-file module file)
               ;; After to avoid short-circuit
               loaded-mod-b)))
      ;; There is no rush to remove module from list
      (bebop--assert-loaded module loaded-mod-b))
    ;; See `bebop--load-module-t'
    (bebop--modules-loading-adjust -1)
    (when bebop-modules-async
      (mutex-unlock (gethash module bebop--module-mutexes)))
    ;; Check to see if the modules have been loaded
    (bebop--modules-loaded)))

(defun bebop--modules-loading-adjust (ment)
  "Increments or decrements `bebop--modules-loading' by `ment'
      (pun intended)"
  (setq bebop--modules-loading
        (+ ment bebop--modules-loading)))

(defun bebop--assert-loaded (module loaded-mod-b)
  "If something was loaded add it to `bebop--unloaded-modules'"
  (if loaded-mod-b
      (push module bebop--loaded-modules)
    (push module bebop--unloaded-modules)))

(defun bebop--load-file (module file)
  "Loads a file for a given module, scanning all modules in
      `bebop--modules-directories' and returns a bool if module was loaded"
  (let (loaded-mod-b)
    ;; Iterate through possible load directories
    (dolist (module-dir (bebop--modules-directories))
      ;; Only if the file has not been loaded
      (when (not loaded-mod-b)
        ;; Remember if a file has been loaded
        (setq loaded-mod-b
              ;; Attempt to load a file
              (load (concat module-dir "/"
                            module "/"
                            (symbol-name file))
                    ;; Don't raise errors, and
                    'missing-ok (not bebop-developer)))))
    ;; `dolist' done, return value
    loaded-mod-b))

(defun bebop/after-module (module)
  (cond
   ((bebop--module-loadedp module) t)
   ((< bebop--modules-total bebop--modules-loading)
    (error "Recursive module dependencies!"))
   ((member module bebop--modules-list)
    (progn
      (if bebop-modules-async
          (let ((wait-mutex (gethash module
                                     bebop--module-mutexes)))
            (mutex-lock wait-mutex)
            (mutex-unlock wait-mutex)
            t)
        (bebop--load-module module))))
   (t nil)))

(defmacro bebop/after-modules (&rest module-ist)
  `(mapc #'bebop//after-module
         (bebop-modules-expand (quote ,module-list))))

(provide 'core-modules)
;;; core-modules.el ends her
