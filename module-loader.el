;;; module-loader.el --- Guile elisp compatible -*- lexical-binding: t; -*-

(defvar bebop--init-directory
  (progn
    (print "Error: bebop--init-directory is not defined - using
  default emacs path.")
    "$HOME/.emacs.d/"))

(defvar bebop-custom-modules nil
  "List of user defined module folders. ")

(defvar bebop--system-module
  (concat bebop--init-directory "modules")
  "The folder of the base system's modules.")

(defun bebop--modules-directories ()
  "List of modules to be loaded."
  (cons
   bebop--system-module
   bebop-custom-modules))

(defmacro bebop-modules (&rest lst)
  `(progn
     (defconst bebop--modules-list (bebop-modules-expand (quote ,lst))
       "List of string representing the format FOLDER/MODULE")
     (defconst bebop--modules-total (length bebop--modules-list))))

(defun bebop-modules-expand (mods-plst)

  (defun plist-to-alist (lst)
    (cond
     ((null lst)
      nil)
     ((listp (car lst))
      (cons (car lst)
            (plist-to-alist (cdr lst))))
     (t
      (cons (cons (car lst) (car (cdr lst)))
            (plist-to-alist (cdr (cdr lst)))))))

  (setq mods-lst (plist-to-alist mods-plst))
  (setq return-list nil)

  (dolist (mods mods-lst)
    (dolist (mod (cdr mods))
      (if (equal ":" (substring (prin1-to-string (car mods)) 0 1))
          (setq return-list (cons (concat
                                   (substring (prin1-to-string (car mods)) 1)
                                   "/"
                                   (prin1-to-string mod))
                                  return-list))
        (print (concat "Error: "
                       (prin1-to-string (car mods))
                       " is not a valid module folder specifier.")))))
  return-list)

;;; module-loader.el ends here
