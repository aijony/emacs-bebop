(use-modules (srfi srfi-1)
             (system base compile))


(define init-directory
  (string-append (if (current-filename)
                     (dirname (current-filename))
                     (canonicalize-path ".")) "/"))

(define (load-elisp-file file)
  (load-compiled (compile-file file
                               ;; #:env (current-module)
                               #:from 'elisp)))

(define (load-elisp expr)
  (compile expr
           ;; #:env (current-module)
           #:from 'elisp))

(load-elisp `(defvar bebop--init-directory ,init-directory))

(load-elisp-file "module-loader.el")
(load-elisp-file "modules.el")

(define module-list (load-elisp 'bebop--modules-list))
(define module-paths (load-elisp 'bebop--modules-directories))

(define manifest-list '())

(define (load-manifest path)
  (let ((file (string-append path "/manifest.scm")))
    (if (file-exists? file)
        (begin
          (map symbol->string (load file)))
        '())))

(define (load-module paths module)
  (if (not (or (null? paths)
               (null? module)))
      (let* ((module-path (string-append
                           (car paths)
                           "/" module))
             (manifest-list (load-manifest module-path)))
        (if (not (null? manifest-list))
            manifest-list
            (load-module (cdr paths) module)))
      '()))

(define (load-modules paths modules)
  (specifications->manifest
   (fold append  '() (map (lambda (module)
                            (load-module paths module))
                          modules))))

(load-modules module-paths module-list)
