;;; early-init.el --- mostly stolen from doom -*- lexical-binding: t; -*-

;; Reduce GC frequency for startup
(setq gc-cons-threshold most-positive-fixnum
      gc-cons-percentage 0.6)

;; Prevent package initialization
(setq package-enable-at-startup nil)
(advice-add #'package--ensure-init-file :override #'ignore)

;; Prevent loading unstyled Emacs UI elements
(push '(menu-bar-lines . 0) default-frame-alist)
(push '(tool-bar-lines . 0) default-frame-alist)
(push '(vertical-scroll-bars) default-frame-alist)

;; Prevent frame resizing
(setq frame-inhibit-implied-resize t)

;; No need to depend on X settings
(advice-add #'x-apply-session-resources :override #'ignore)

;; Guix has forces a site-file full of autoloads, but Emacs Bebop
;; handles needs to be a bit more lazy
(setq site-run-file nil)

(defconst bebop--init-directory (expand-file-name user-emacs-directory)
  "The abosulte path of the directory containing init.el
  (e.g. \"/home/user/.emacs.d/\")")

;; Guix profile environment variable
;; We may delete this - it didn't do anything
(setenv "XDG_DATA_DIRS" (concat (getenv "XDG_DATA_DIRS")
                                ":"
                                bebop--init-directory

                                "guix/profile/share"))
