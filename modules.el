(bebop-modules
 :completion
 (ivy)

 :correction
 (format
  text)

 :emacs
 (defaults)

 :keybindings
 (commands
  evil)

 :language
 (elisp
  haskell
  latex
  ledger
  lisp
  octave
  org
  scheme)

 :tools
 (git)

 :system
 (guix)

 :bebop
 (core))
