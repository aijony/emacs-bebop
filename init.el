;;; init.el -*- lexical-binding: t; -*-

;;; Performance
(defvar bebop-gc-cons-threshold 16777216 ; 16mb
  "The default value to use for `gc-cons-threshold'. If you experience
freezing, decrease this. If you experience stuttering, increase this.")

;; Backup "magic" that allows opening compressed and remove files
(defvar bebop--file-name-handler-alist file-name-handler-alist)

;; No "magic"
(setq file-name-handler-alist nil)

;; Don't blind the user with a flash of white
(set-background-color "#000000")

(defun bebop--restore-variables ()
  "Restore any values that from init back to a normal preference"
  (setq file-name-handler-alist bebop--file-name-handler-alist
        gc-cons-threshold bebop-gc-cons-threshold
        gc-cons-percentage 0.1))

;; Return the "magic" and GC
(add-hook 'emacs-startup-hook #'bebop--restore-variables)

;;; Loading

;; Don't load stale byte-code on non-user runs
(setq load-prefer-newer noninteractive)

;; Don't add that `custom-set-variables' block to my init.el!
(setq package--init-file-ensured t)
(defgroup bebop-variables '() "Emacs Bebop Variables")

(defconst bebop--core-dir (concat bebop--init-directory "core/"))

(defun load-config-file (file)
  (load (concat bebop--init-directory file) nil 'nomessage))

(defun load-core-file (file)
  (load (concat bebop--core-dir file) nil 'nomessage))

(load-core-file "core-init")

;; Liftoff!
(bebop--initialize-core-t)
