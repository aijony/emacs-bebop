
;; Simple Magit
(use-package magit
  :general
  (bebop-leader-def
    "gg" 'magit))
