;; TODO Export format// to minor mode repo

(defun format//indent-buffer ()
  (interactive)
  "Indents the buffer."
  (save-excursion
    (indent-region (point-min) (point-max) nil)))

(defun format//cleaner (buffer)
  "Indents a given `buffer' without messages or undo information. This
is meant to run as a thread."
  (setq format//clean-in-progress t)
  (bebop//ghost
   (with-current-buffer buffer
     (format//indent-buffer)))
  (setq format//clean-in-progress nil))

(defun format//clean-buffer ()
  "Asynchronously formats the buffer with `format//cleaner'"
  (interactive)
  (if (not (or undo-in-progress
               format//clean-in-progress
               buffer-read-only
               (member last-command format//last-command-ignore)))
      (make-thread
       (apply-partially #'format//cleaner (current-buffer)))))

(defun format/toggle (&optional state)
  (interactive)
  "Toggle format module facilities such as auto-indentation on
  input. STATE will enable or disable for values of 0 and 1
  respectively. Returns the value of `format//cleaner-state'."
  ;; Set the format state to given state or opposite of current
  (setq-local format//cleaner-state (if state
                                        state
                                      ;; Swaps 1 and 0
                                      (abs (- 1 format//cleaner-state))))
  ;; Add or remove hook based off of state
  (if (eq format//cleaner-state 1)
      (add-hook 'post-command-hook #'format//post-command-hook t t)
    (remove-hook 'post-command-hook #'format//post-command-hook t))
  format//cleaner-state)

(defun format//post-command-hook ()
  "Things to run on post-command-hook"
  ;; This will not stop the thread
  (while-no-input
    ;; There should only be one `format//time'
    (if format//timer
        (cancel-timer format//timer))
    (setq-local format//timer
                (run-with-idle-timer format/idle-time
                                     nil
                                     #'format//clean-buffer))))

(defun format/show-trailing-whitespace (&optional pass)
  "Toggles `show-trailing-whitespace' or sets to PASS."
  (interactive)
  (setq-local show-trailing-whitespace (if pass
                                           pass
                                         (not show-trailing-whitespace))))
