
(use-package whitespace-cleanup-mode
  :hook (text-mode prog-mode))

(use-package editorconfig
  :hook (prog-mode . editorconfig-mode)
  :config
  (setq editorconfig-trim-whitespace-mode
	'whitespace-cleanup-mode))


