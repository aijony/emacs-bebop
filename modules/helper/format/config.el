
(defvar format//clean-in-progress nil
  "Buffer local variable to keep track of a single instance of
  `format//cleaner'")
(defvar format//timer nil
  "Buffer local timer to keep track of one cleaner waiting at a time")
(defvar format//cleaner-state 0
  "Variable to keep track of whether format module is enabled.")

(defvar format//last-command-ignore '(undo
                                      undo-tree-undo
                                      format//clean-buffer)
  "No formatting done immediately after these commands")

(defvar format/idle-time .1
  "How many seconds to wait to run `format//cleaner'")


(setq-default show-trailing-whitespace nil)

(add-hook 'prog-mode-hook #'format/toggle)
(add-hook 'prog-mode-hook #'format/show-trailing-whitespace)
(add-hook 'prog-mode-hook #'auto-fill-mode)

(setq-default indent-tabs-mode nil)
