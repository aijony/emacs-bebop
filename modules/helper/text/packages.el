(defvar spell-checking-enable-auto-dictionary nil
  "Specify if auto-dictionary should be enabled or not.
TODO: Make emacs-auto-dictionary package")

(use-package auto-dictionary
  :if spell-checking-enable-auto-dictionary
  :commands adict-change-dictionary
  :hook (flyspell-mode . auto-dictionary-mode))

(use-package flyspell-correct
  :general
  (bebop-leader-def
    "Sc" '(flyspell-correct-previous-word-generic
           :which-key "correct word")))

(use-package flyspell-correct-ivy
  :after ivy
  :commands (flyspell-correct-ivy)
  :init
  (setq flyspell-correct-interface #'flyspell-correct-ivy))

(use-package flyspell
  :hook ((text-mode . flyspell-mode)
         (prog-mode . flyspell-prog-mode))
  :init
  ;; Pre-initialize variables and dicts alists
  ;; This is preferred on startup as most sessions require
  ;; spell-checking if this module is enabled and will lessen initial
  ;; file load times
  (with-temp-buffer
    (bebop//ghost (flyspell-mode))))
