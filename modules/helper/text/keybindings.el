
(bebop-leader-def
  ;; Spelling
  "Sb" 'flyspell-buffer
  "Sd" '(text/change-dictionary
         :which-key "change dictionary")
  "Sn" '(flyspell-goto-next-error
         :which-key "goto typo"))
