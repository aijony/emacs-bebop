
;; For recentf changes
(bebop/after-module "emacs/defaults")

(use-package ivy
  :config
  (ivy-mode 1)
  (setq
   enable-recursive-minibuffers t
   ivy-use-virtual-buffers t
   ;; Prevents auto-completion when making a new file/dir
   ivy-magic-slash-non-match-action 'ivy-magic-slash-non-match-create
   ivy-height 20)

  (bebop-leader-def
    ;; Completion
    "cr" 'ivy-resume
    ;; Buffer
    "bb" 'ivy-switch-buffer)

  ;; Evil keybindings for Ivy buffers
  (general-define-key
   :states 'normal
   :keymaps 'ivy-minibuffer-map
   "H" 'delete-minibuffer-contents
   "h" 'backward-char
   "l" 'forward-char
   "L" 'ivy-insert-current
   "j" 'ivy-next-line
   "J" 'ivy-next-history-element
   "k" 'ivy-previous-line
   "K" 'ivy-previous-history-element
   "gg"'ivy-beginning-of-buffer
   "G" 'ivy-end-of-buffer
   "/" 'ivy-reverse-i-search
   "Q" 'exit-minibuffer
   "RET" 'ivy-done
   "DEL" 'ivy-backward-delete-char
   "S-RET"    'ivy-insert-current
   "<escape>" 'kill-this-buffer)

  ;; Mirror Evil in insert with control
  (general-define-key
   :states 'insert
   :keymaps 'ivy-minibuffer-map
   "C-j" 'ivy-next-line
   "C-k" 'ivy-previous-line
   "C-w" 'ivy-yank-word
   "C-W" 'ivy-yank-symbol
   "C-l" 'ivy-yank-char
   "C-q" 'exit-minibuffer
   "DEL" 'ivy-backward-delete-char
   ;; Do not taint blessed C-g
   "C-/"   'ivy-reverse-i-search
   "C-S-RET" 'ivy-insert-current))

(use-package counsel
  :after ivy
  :config
  (counsel-mode 1)
  ;; Minibuffer History
  (general-define-key
   :states '(insert normal)
   :keymaps 'ivy-minibuffer-map
   "<backtab>" 'counsel-minibuffer-history)
  ;; Find-file specifics
  (general-define-key
   :states 'normal
   :keymaps 'counsel-find-file-map
   "L" 'ivy//right-helper
   "H" 'ivy//left-helper)
  (general-define-key
   :state 'emacs
   "M-x" 'counsel-M-x)
  ;; Counsel unique applications
  (bebop-leader-def
    "SPC" '(counsel-M-x :which-key "M-x")
    "xu" 'counsel-unicode-char
    "co" 'counsel-outline
    "fb" 'counsel-bookmark
    "gf" 'counsel-git
    "gG" 'counsel-git-grep))

(use-package swiper
  :init
  ;; Include letters with diacritical marks
  (setq search-default-mode #'char-fold-to-regexp)
  :general
  (bebop-leader-def
    "/" 'swiper))

(use-package avy
  :commands ivy-avy
  :general
  (bebop-leader-def
    ":" 'avy-goto-char))
