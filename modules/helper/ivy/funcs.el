
(defun ivy//go-past-dir-dots ()
  "Given:
   >.
    ..
    file
   Go to:
    .
    ..
   >file"
  (dotimes (n 2)
    (ivy-next-line)))


(defun ivy//left-helper ()
  "In find-file go back a directory"
  (interactive)
  (let
      ;; Emulate a buffer to get `ivy-insert-current-full'
      ((full-path (with-temp-buffer
		    (ivy-insert-current-full)
		    (buffer-string))))
    (delete-minibuffer-contents)
    (ivy-backward-kill-word)
    ;; Get the second to last element in list
    (insert (car (last (split-string full-path "\/") 2)))
    (ivy//go-past-dir-dots)))

(defun ivy//right-helper ()
  "In find-file go forward a directory"
  (interactive)
  (ivy-insert-current)
  (ivy-partial)
  (ivy//go-past-dir-dots))
