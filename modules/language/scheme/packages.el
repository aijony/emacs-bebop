(use-package scheme-mode
  :mode ("\\.scm\\'" "\\.sls\\'" "\\.ss\\'" "\\.sld\\'"))

(use-package geiser
  :after scheme
  :init
  (setq geiser-active-implementations '(guile))
  :config
  (load "geiser-autoloads" nil t)

  (bebop-local-leader-def scheme-mode-map
    "'"  'geiser-mode-switch-to-repl

    "c"  '(:ignore t :which-key "compile")
    "cc" 'geiser-compile-current-buffer
    "cp" 'geiser-add-to-load-path

    "e"  '(:ignore t :which-key "eval")
    "eb" 'geiser-eval-buffer
    "ee" 'geiser-eval-last-sexp
    "ef" 'geiser-eval-definition
    "el" 'lisp-state-eval-sexp-end-of-line
    "er" 'geiser-eval-region

    "g"  '(:ignore t :which-key "nav")
    "gb" 'geiser-pop-symbol-stack
    "gm" 'geiser-edit-module
    "gn" 'next-error
    "gN" 'previous-error

    "h"  '(:ignore t :which-key "doc")
    "hh" 'geiser-doc-symbol-at-point
    "hd" 'geiser-doc-look-up-manual
    "hm" 'geiser-doc-module
    "h<" 'geiser-xref-callers
    "h>" 'geiser-xref-callees

    "i"  '(:ignore t :which-key "insert")
    "il" 'geiser-insert-lambda

    "m"  '(:ignore t :which-key "expand")
    "me" 'geiser-expand-last-sexp
    "mf" 'geiser-expand-definition
    "mx" 'geiser-expand-region

    "s"  '(:ignore t :which-key "repl")
    "si" 'geiser-mode-switch-to-repl
    "sb" 'geiser-eval-buffer
    "sB" 'geiser-eval-buffer-and-go
    "sf" 'geiser-eval-definition
    "sF" 'geiser-eval-definition-and-go
    "se" 'geiser-eval-last-sexp
    "sr" 'geiser-eval-region
    "sR" 'geiser-eval-region-and-go
    "ss" 'geiser-set-scheme))
