
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package lispy
  :commands lispy-mode
  :hook ((lisp-mode
          clojure-mode
          scheme-mode
          racket-mode
          common-lisp-mode
          emacs-lisp-mode)
         . lisp//enable-lispy)
  (minibuffer-setup . lisp//conditionally-enable-lispy))

(use-package lispyville
  :after evil
  :hook
  (lispy-mode . lispyville-mode)
  :config
  (lispy-set-key-theme '(c-digits))
  (lispyville-set-key-theme
   '(operators
     c-w
     (escape insert emacs)
     (additional-movement normal visual motion))))
