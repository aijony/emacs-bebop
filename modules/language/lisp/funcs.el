
(defun lisp//enable-emacs-parenthesis ()
  "Enables built-in modes for helping deal with parenthesis."
  (show-paren-mode 1))

(defun lisp//conditionally-enable-lispy ()
  (when (eq this-command 'eval-expression)
    (lisp//enable-lispy)))

(defun lisp//enable-lispy ()
  "Avoid using lambdas with this helper function to enable
`lispy-mode'"
  (lispy-mode 1))

(defun lisp//disable-lispy ()
  "Avoid using lambdas with this helper function to disable
  `lispy-mode'"
  (lispy-mode -1))

(with-eval-after-load 'lispy
  (defun lispy-wrap-round (arg)
    "Forward to `lispy-parens' with a default ARG of 0."
    (interactive "P")
    (lispy-parens (or arg 0)))

  (defun lispy-wrap-brackets (arg)
    "Forward to `lispy-brackets' with a default ARG of 0."
    (interactive "P")
    (lispy-brackets (or arg 0)))

  (defun lispy-wrap-braces (arg)
    "Forward to `lispy-braces' with a default ARG of 0."
    (interactive "P")
    (lispy-braces (or arg 0))))
