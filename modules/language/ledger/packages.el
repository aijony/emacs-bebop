
(use-package ledger-mode
  :mode ("\\.dat\\'"
         "\\.ledger\\'")
  :config
  (bebop-local-leader-def ledger-report-mode-map
    "q"   #'ledger-report-quit
    "RET" #'ledger-report-edit-report
    "gd"  #'ledger-report-visit-source
    "gr"  #'ledger-report-redo)
  (bebop-local-leader-def ledger-mode-map
    "N" #'ledger-navigate-next-xact-or-directive
    "n" #'ledger-navigate-prev-xact-or-directive
    "a" #'ledger-add-transaction
    "t" #'ledger-toggle-current
    "d" #'ledger-delete-current-transaction
    "r" #'ledger-report
    "R" #'ledger-reconcile
    "s" #'ledger-sort-region
    "S" #'ledger-schedule-upcoming
    "gs" #'ledger-display-ledger-stats
    "gb" #'ledger-display-balance-at-point))
