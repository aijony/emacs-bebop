(use-package octave
  :mode ("\\.m\\'" . octave-mode)
  :commands (run-octave)
  :init
  (setq octave-comment-char bebop/octave-comment-char)
  :general
  (bebop-local-leader-def octave-mode-map
    "'"  'run-octave
    "e"  '(:ignore t :which-key "eval")
    "eb" 'octave-send-buffer
    "ef" 'octave-send-defun
    "el" 'octave-send-line
    "er" 'octave-send-region

    "i"  '(:ignore t :which-key "insert")
    "if" 'octave-insert-defun

    "h"  '(:ignore t :which-key "doc")
    "hh" 'octave-help
    "hh" 'octave-find-definition
    "hl" 'octave-lookfor)
  :config
  (defun octave-indent-comment ()
    "A function for `smie-indent-functions' (which see).

     OVERRIDE: Make Octave comments aligned to text"
    (save-excursion
      (back-to-indentation)
      (cond
       ((octave-in-string-or-comment-p) nil)
       ((looking-at-p "\\(\\s<\\)\\1\\{2,\\}")
        0)
       ;; Exclude %{, %} and %!.
       ((and (looking-at-p "\\s<\\(?:[^{}!]\\|$\\)")
             (not (looking-at-p "\\(\\s<\\)\\1")))
        nil))))

  (setq octave-block-offset 2))

(use-package matlab
  :after octave
  :commands matlab-mode
  :if bebop/matlab-support
  :general
  (bebop-local-leader-def octave-mode-map
    "M" 'matlab-mode)
  (bebop-local-leader-def matlab-mode-map
    "O"  'octave-mode)
  :config
  (setq matlab-mode-hook prog-mode-hook))
