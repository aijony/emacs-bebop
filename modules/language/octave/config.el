(defvar bebop/matlab-support nil
  "Enables matlab-mode usage from octave-mode")

(defvar bebop/octave-comment-char ?#
  "If non-nil sets `octave-comment-char' before `octave-mode' loads. A
  good example would be (`setq' `bebop/octave-comment-char' ?%)")
