

(defvar org/evil-org-themes '(navigation
                              insert
                              textobjects
                              additional
                              calendar))

(defun org/evil-org-mode-h ()
  "Configures `evil-org-mode' by setting its themes"
  (evil-org-set-key-theme org/evil-org-themes))
