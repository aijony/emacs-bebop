
(use-package org
  :commands org-mode
  :config

  (org-babel-do-load-languages 'org-babel-load-languages
                               '((shell . t)
                                 (dot . t)))

  (bebop-local-leader-def org-mode-map
    "'" 'org-edit-special
    "," 'org-switchb

    ;; "." #'org-goto
    ;; TODO: Modularize
    "." 'counsel-org-goto
    "/" 'counsel-org-goto-all

    "A" 'org-archive-subtree
    "d" 'org-deadline
    "e" 'org-export-dispatch
    "f" 'org-footnote-new
    "h" 'org-toggle-heading
    "i" 'org-toggle-item
    "I" 'org-toggle-inline-images
    "n" 'org-store-link
    "o" 'org-set-property
    "p" 'org-priority
    "q" 'org-set-tags-command
    "s" 'org-schedule
    "t" 'org-todo
    "T" 'org-todo-list

    ;; Attachments
    "a"  '(:ignore t :which-key "attachments")
    "aa" 'org-attach
    "ad" 'org-attach-delete-one
    "aD" 'org-attach-delete-all
    "an" 'org-attach-new
    "ao" 'org-attach-open
    "aO" 'org-attach-open-in-emacs
    "ar" 'org-attach-reveal
    "aR" 'org-attach-reveal-in-emacs
    "au" 'org-attach-url
    "as" 'org-attach-set-directory
    "aS" 'org-attach-sync

    ;; Tables
    "ab" '(:ignore t :which-key "tables")
    "a-" 'org-table-insert-hline
    "aa" 'org-table-align
    "ac" 'org-table-create-or-convert-from-region
    "ae" 'org-table-edit-field
    "ah" 'org-table-field-info

    ;; Clck
    "c"  '(:ignore t :which-key "clock")
    "cc" 'org-clock-in
    "cC" 'org-clock-out
    "cd" 'org-clock-mark-default-task
    "ce" 'org-clock-modify-effort-estimate
    "cE" 'org-set-effort
    "cl" 'org-clock-in-last
    "cg" 'org-clock-goto
    "cr" 'org-clock-report
    "cx" 'org-clock-cancel
    "c=" 'org-clock-timestamps-up
    "c-" 'org-clock-timestamps-down

    "g"  '(:ignore t :which-key "goto")
    ;; "g" #'org-goto
    ;; TODO: Modularize
    "gg" 'counsel-org-goto
    "gG" 'counsel-org-goto-all

    ;; Links
    "l"  '(:ignore t :which-key "links")
    "lc" 'org-cliplink
    "ll" 'org-insert-link
    "lL" 'org-insert-all-links
    "ls" 'org-store-link
    "lS" 'org-insert-last-stored-link
    "li" 'org-id-store-link

    ;; Refile
    "r"  '(:ignore t :which-key "refile")
    "rr" 'org-refile))

(use-package org-bullets
  :after org
  :config
  (add-hook 'org-mode-hook #'org-bullets-mode))

(use-package ivy-omni-org
  :after (org ivy)
  :commands ivy-omni-org
  :config
  (setq ivy-omni-org-file-sources '(org-agenda-files)))

(use-package evil-org
  :after (org evil)
  :config
  (add-hook 'org-mode-hook 'evil-org-mode)
  (add-hook 'evil-org-mode-hook 'org/evil-org-mode-h))

(use-package evil-org-agenda
  :after evil-org
  :config
  (evil-org-agenda-set-keys)

  (bebop-local-leader-def
    :keymaps 'org-agenda-mode-map
    "d"  'org-agenda-deadline
    "c"  '(:ignore t :which-key "clock")
    "cc" 'org-agenda-clock-in
    "cC" 'org-agenda-clock-out
    "cg" 'org-agenda-clock-goto
    "cr" 'org-agenda-clockreport-mode
    "cs" 'org-agenda-show-clocking-issues
    "cx" 'org-agenda-clock-cancel
    "cq" 'org-agenda-set-tags
    "cr" 'org-agenda-refile
    "cs" 'org-agenda-schedule
    "ct" 'org-agenda-todo))
