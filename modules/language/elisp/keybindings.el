
(bebop-local-leader-def emacs-lisp-mode-map
  "e"  '(:ignore t :which-key "eval")
  "ee"  'eval-last-sexp
  "er"  'eval-region
  "ef"  'eval-defun
  "eE"  'eval-expression
  "ep"  'eval-print-last-sexp
  "eb"  'eval-buffer)
