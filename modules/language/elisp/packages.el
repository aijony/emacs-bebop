
(use-package elisp-demos
  :config
  (advice-add 'helpful-update :after
              #'elisp-demos-advice-helpful-update))

(use-package elisp-refs
  :general
  (bebop-local-leader-def emacs-lisp-mode-map
    "r"  '(:ignore t :which-key "ref")
    "rf" 'elisp-ref-functions
    "rm" 'elisp-ref-macro
    "rv" 'elisp-ref-variable
    "rs" 'elisp-ref-symbol
    "rs" 'elisp-ref-special))

(use-package elisp-slime-nav
  :hook ((emacs-lisp-mode ielm-mode) . elisp-slime-nav-mode))

(use-package suggest)
