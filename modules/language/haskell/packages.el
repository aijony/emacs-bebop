(use-package haskell-mode
  :mode ("\\.hs\\'"))

(use-package hlint-refactor
  :after haskell-mode)

(use-package dante
  :after haskell-mode
  :commands 'dante-mode
  :init
  ;; (add-hook 'haskell-mode-hook 'flycheck-mode)
  ;; OR for flymake support:
  ;; (add-hook 'haskell-mode-hook 'flymake-mode)
  ;; (remove-hook 'flymake-diagnostic-functions 'flymake-proc-legacy-flymake)
  :config
  ;; (flycheck-add-next-checker 'haskell-dante '(info . haskell-hlint))
  (add-hook 'haskell-mode-hook 'dante-mode))
