
;; Common Prefixes
(bebop-leader-def
  ";"  '(:ignore t :which-key "comments")
  "a"  '(:ignore t :which-key "apps")
  "b"  '(:ignore t :which-key "buffer")
  "B"  '(:ignore t :which-key "bookmarks")
  "c"  '(:ignore t :which-key "completion")
  "e"  '(:ignore t :which-key "emacs")
  "ed" '(:ignore t :which-key "describe")
  "eF" '(:ignore t :which-key "face")
  "el" '(:ignore t :which-key "load")
  "et" '(:ignore t :which-key "themes")
  "f"  '(:ignore t :which-key "files")
  "fv" '(:ignore t :which-key "variable")
  "g"  '(:ignore t :which-key "git")
  "l"  '(:ignore t :which-key "lisp")
  "S"  '(:ignore t :which-key "spelling")
  "w"  '(:ignore t :which-key "windows")
  "x"  '(:ignore t :which-key "text")
  "xa" '(:ignore t :which-key "align")
  "xd" '(:ignore t :which-key "delete")
  "xj" '(:ignore t :which-key "justify")
  "xl" '(:ignore t :which-key "lines")
  "xt" '(:ignore t :which-key "transpose"))

;; Comments
(bebop-leader-def
  ";l" 'comment-or-uncomment-region-or-line)

;; Eshell
(bebop-leader-def
  "'" 'eshell)

;; Applications
(bebop-leader-def
  "ac" 'calc-dispatch)

;; Buffers
(bebop-leader-def
  "bi" 'imenu
  "bn" 'next-buffer
  "bp" 'previous-buffer
  "bw" 'read-only-mode
  "bd" 'kill-this-buffer
  "bk" 'kill-buffer)

;; Bookmarks
(bebop-leader-def
  "Bj" 'bookmark-jump)

;; Files
(bebop-leader-def
  "ff" 'find-file
  "fl" 'find-file-literally
  "fs" 'rgrep
  "fw" 'save-buffer
  ;; Local Variable
  "fvd" 'add-dir-local-variable
  "fvf" 'add-file-local-variable
  "fvp" 'add-file-local-variable-prop-line)

;; Emacs
(bebop-leader-def
  "en" 'view-emacs-news
  "es" 'info-lookup-symbol
  "eh" 'help
  "ea" 'apropos-command
  "ef" 'toggle-frame-fullscreen
  ;; Library
  "elf" 'find-library
  "ell" 'load-library
  ;; hHeme
  "etd" 'describe-theme
  "etl" 'load-theme
  ;; Face
  "eFd" 'describe-face
  "eFl" 'list-faces-display
  ;; Describe
  "edb" 'describe-bindings
  "edc" 'describe-char
  "edf" 'describe-function
  "edF" 'describe-face
  "edk" 'describe-key
  "edp" 'describe-package
  "edt" 'describe-theme
  "edv" 'describe-variable)



;; Windows
(bebop-leader-def
  "wf" 'follow-mode
  "wF" 'make-frame
  "wo" 'other-frame
  "ws" 'split-window-below
  "wS" 'split-window-below-and-focus
  "w-" 'split-window-below
  "wU" 'winner-redo
  "wu" 'winner-undo
  "wv" 'split-window-right
  "wV" 'split-window-right-and-focus
  "ww" 'other-window
  "w/" 'split-window-right
  "w=" 'balance-windows)

;; Text
(bebop-leader-def
  ;; Align
  "xaa" 'align
  "xac" 'align-current
  "xc"  'count-region
  "xdw" 'delete-trailing-whitespace
  ;; Justify
  "xjc" 'set-justification-center
  "xjf" 'set-justification-full
  "xjl" 'set-justification-left
  "xjn" 'set-justification-none
  "xjr" 'set-justification-right
  ;; Transpose
  "xtc" 'transpose-chars
  "xtl" 'transpose-lines
  "xtw" 'transpose-words
  "x TAB" 'indent-rigidly)
