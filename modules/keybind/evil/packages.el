
;;; EVIL (mwhaha!)

(use-package evil
  :init
  (setq evil-want-keybinding nil
	evil-want-minibuffer t)
  :config
  (evil-mode 1)
  ;; Evil doesn't remember to go into insert state
  ;; every time the minibuffer is setup.
  (add-hook 'minibuffer-setup-hook #'evil-insert-state)

  ;;; `general' related settings

  ;; Inhibit message if not debugging
  (if bebop-developer
      (key-chord-mode 1)
    (bebop//ghost (key-chord-mode 1)))

  (setq key-chord-two-keys-delay 0.05)
  (general-evil-setup)
  ;; Quick escape to 'kj'
  (general-define-key :states '(insert
                                emacs
                                visual)
                      (general-chord "jk") 'evil-normal-state
                      (general-chord "kj") 'evil-normal-state)

  (bebop-leader-def
    ;; Emacs
    ;; TODO Make this a toggle
    "ee" 'evil-emacs-state
    ;; Files
    "fW" 'evil-write-all
    ;; Windows
    "wH" 'evil-window-move-far-left
    "wd" 'evil-window-delete
    "wh" 'evil-window-left
    "wJ" 'evil-window-move-very-bottom
    "wj" 'evil-window-down
    "wK" 'evil-window-move-very-top
    "wk" 'evil-window-up
    "wL" 'evil-window-move-far-right
    "wl" 'evil-window-right))

(use-package undo-tree
  :config
  (bebop-leader-def
    ;; Apps
    "au" 'undo-tree-visualize))

(use-package evil-collection
  :after evil
  :init
  ;; This is optional since it's already set to t by default.
  (setq evil-want-integration t)
  :config
  (evil-collection-init))

(use-package evil-exchange
  :after evil
  :config
  (evil-exchange-install))

(use-package evil-surround
  :config
  (global-evil-surround-mode 1))

(use-package evil-matchit
  :config
  (global-evil-matchit-mode 1))
