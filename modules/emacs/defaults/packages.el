
(use-package no-littering
  :init
  (setq no-littering-etc-directory bebop-etc-directory
  	no-littering-var-directory bebop-var-directory)
  :config
  (setq auto-save-list-file-prefix
	(concat bebop-var-directory "auto-save-list/.saves-")
	auto-save-list-file-name
	(concat auto-save-list-file-prefix
		(number-to-string (emacs-pid))
		"-" (system-name) "~"))

  (setq auto-save-file-name-transforms
	`((".*" ,bebop--auto-save-directory t)))

  (setq backup-directory-alist
	`(("." . ,bebop--backup-directory)))

  (bebop//ghost (recentf-mode 1)))

