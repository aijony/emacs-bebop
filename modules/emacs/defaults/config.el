(defvar bebop-var-directory
  (concat (expand-file-name user-emacs-directory)
          "var/")
  "Messy files generated by Emacs go in here (e.g. logs, autosaves).
See `no-littering-var-directory'.")

(defvar bebop-etc-directory
  (concat (expand-file-name user-emacs-directory)
          "etc/")
  "Messy configs generated by Emacs go in here (e.g. custom.el, yasnippets).
See `no-littering-etc-directory'.")

(defvar custom-file (concat bebop-etc-directory "custom.el"))

(defvar bebop--auto-save-directory
  (concat bebop-var-directory "auto-saves/")
  "Auto-save files go here")

(defvar bebop--backup-directory
  (concat bebop-var-directory "backups/")
  "Auto-save files go here")

(defvar bebop--auto-save-list-directory
  (concat bebop-var-directory "auto-save-list/")
  "Auto-save lists go here")

(defvar bebop--directories (list bebop-etc-directory
                                 bebop-var-directory
                                 bebop--auto-save-list-directory
                                 bebop--auto-save-directory
                                 bebop--backup-directory)
  "Directories to make sure exist at start-up.")

;; Check if directories exist
(bebop--check-directories bebop--directories)

;; TODO:f Disable for gpg files
;; Backup of a file the first time it is saved.
(setq make-backup-files t
      ;; Don't clobber symlinks
      backup-by-copying t
      ;; Version numbers for backup files
      version-control t
      ;; Delete excess backup files silently
      delete-old-versions t
      delete-by-moving-to-trash t
      ;; Oldest versions to keep when a new numbered backup is made (default: 2)
      kept-old-versions 6
      ;; newest versions to keep when a new numbered backup is made (default: 2)
      kept-new-versions 9
      ;; auto-save every buffer that visits a file
      auto-save-default t
      ;; Number of seconds idle time before auto-save (default: 30)
      auto-save-timeout 20
      ;; Number of keystrokes between auto-saves (default: 300)
      auto-save-interval 200)
