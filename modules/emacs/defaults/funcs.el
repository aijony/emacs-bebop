
(defun bebop--check-directories (dirs)
  "Check if every directory in `dirs' exists,
and if not make the directory."
  (dolist (dir dirs)
    (if (not (file-exists-p dir))
        (make-directory dir t))))

