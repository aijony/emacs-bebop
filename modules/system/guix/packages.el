
(use-package geiser
  :commands run-geiser
  :config
  (load "geiser-autoloads" nil t))

(use-package guix
  :general
  (bebop-leader-def
    "ag" 'guix)
  :config
  ;; For those of us who do not run Guix's site-init.el
  (autoload 'guix "guix-autoloads" nil t))
