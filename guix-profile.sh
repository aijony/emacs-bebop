#!/bin/sh

guix package --manifest=load-manifests.scm \
     --profile=guix/profile
# --delete-generations=31d \

guix shell  --profile=$HOME/.emacs.d/guix/profile -- emacs
